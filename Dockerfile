FROM golang

WORKDIR /workspace

COPY . .

RUN CGO_ENABLED=0 go build -o /main

FROM scratch

COPY --from=0 /main /main

ENTRYPOINT [ "/hello" ]
